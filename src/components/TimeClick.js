import { Component } from "react";

class TimeClick extends Component {
    constructor(props) {
        super(props)
        this.state = {
            date: []
        }
    }

    clickMeHandler = () => {
        const time = new Date().toLocaleTimeString();
        this.setState({
            date: [...this.state.date, time]
        })
    }

    render() {
        const dataUpdate = this.state.date;
        return (
            <>
                <button onClick={this.clickMeHandler}>Click Me</button>
                
                    <ul>
                        {dataUpdate.map((value, index) => {
                            return <li key={index}> {value} </li>
                        })}
                    </ul>
                

            </>
        )
    }
}

export default TimeClick;